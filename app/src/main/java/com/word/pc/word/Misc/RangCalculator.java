package com.word.pc.word.Misc;

import android.app.Activity;

import com.word.pc.word.R;

public class RangCalculator {

    public static int THEME_VER = 0;

    public static void onActivityCreateSetTheme(Activity activity,int a)
    {
        THEME_VER = a;
        switch (THEME_VER)
        {
            default:
            case 0:
                activity.setTheme(R.style.AppTheme);
                break;
            case 1:
                activity.setTheme(R.style.AppTheme2);
                break;
        }
    }


    public static final String setRangName(int rangg) {
        String RankName = "";

        if (rangg >= 0 && rangg < 5) {
            RankName = "Apprentice";
        } else if (rangg > 5 && rangg < 15) {
            RankName = "Attendant";
        } else if (rangg > 15 && rangg < 30) {
            RankName = "Secretary";
        } else if (rangg > 30 && rangg < 50) {
            RankName = "Master Chief";
        } else if (rangg > 50 && rangg < 70) {
            RankName = "Librarian";
        } else if (rangg > 70 && rangg < 90) {
            RankName = "Vizier";
        } else if (rangg > 90 && rangg < 120) {
            RankName = "Duke";
        } else if (rangg > 120 && rangg < 150) {
            RankName = "High Shaman";
        } else if (rangg > 150 && rangg < 200) {
            RankName = "Attendant";
        } else if (rangg > 200 && rangg < 250) {
            RankName = "General";
        } else if (rangg > 250 && rangg < 300) {
            RankName = "Grand Admiral";
        } else if (rangg > 300 && rangg < 400) {
            RankName = "Royal Knight";
        } else if (rangg > 400 && rangg < 550) {
            RankName = "Royal Councillor";
        } else if (rangg > 550 && rangg < 700) {
            RankName = "Royal Strategos";
        } else if (rangg > 700 && rangg < 900) {
            RankName = "Royal Spokesman";
        } else if (rangg > 900 && rangg < 1000) {
            RankName = "Royal Advisor";
        } else if (rangg > 1000 && rangg < 1300) {
            RankName = "Royal Executor";
        } else if (rangg > 1300 && rangg < 1600) {
            RankName = "King's Advisor";
        } else if (rangg > 1600 && rangg < 2000) {
            RankName = "Hand of the King";
        } else if (rangg > 2000 && rangg < 3000) {
            RankName = "Prime Minister";
        } else if (rangg > 3000) {
            RankName = "Dragonlord";
        }

        return RankName;
    }

    public static final int setMaxScore(int rangg) {
        int maxScore = 0;

        if (rangg < 5) {
            maxScore = 5;
        } else if (rangg < 15) {
            maxScore = 15;
        } else if (rangg < 30) {
            maxScore = 30;
        } else if ( rangg < 50) {
            maxScore = 50;
        } else if (rangg < 70) {
            maxScore = 70;
        } else if (rangg >= 70 && rangg < 90) {
            maxScore = 90;
        } else if (rangg >= 90 && rangg <= 120) {
            maxScore = 120;
        } else if (rangg >= 120 && rangg <= 150) {
            maxScore = 150;
        } else if (rangg >= 150 && rangg <= 200) {
            maxScore = 200;
        } else if (rangg >= 200 && rangg <= 250) {
            maxScore = 250;
        } else if (rangg >= 250 && rangg <= 300) {
            maxScore = 300;
        } else if (rangg >= 300 && rangg <= 400) {
            maxScore = 400;
        } else if (rangg >= 400 && rangg <= 550) {
            maxScore = 550;
        } else if (rangg >= 550 && rangg <= 700) {
            maxScore = 700;
        } else if (rangg >= 700 && rangg <= 900) {
            maxScore = 900;
        } else if (rangg >= 900 && rangg <= 1000) {
            maxScore = 1000;
        } else if (rangg > 1000 && rangg < 1300) {
            maxScore = 1300;
        } else if (rangg > 1300 && rangg < 1600) {
            maxScore = 1600;
        } else if (rangg > 1600 && rangg < 2000) {
            maxScore = 2000;
        } else if (rangg > 2000 && rangg < 3000) {
            maxScore = 3000;
        } else if (rangg > 3000) {
            maxScore = 3000;
        }

        return maxScore;
    }

    public static final int slike[] = new int []{R.drawable.download,R.drawable.download2,R.drawable.images};

    public static String[] proper_noun = {"Angry", "Bored", "Creey", "Hungry","Jealous","Lazy","Arrogant","Clumsy"};
    public static String[] proper_noun2 = {"Baboon", "Bat", "Bear", "Beaver","Bee","Coyote","Duck","Ferret","Iguana"};



}

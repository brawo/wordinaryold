package com.word.pc.word.Aktivnosti;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.tooltip.Tooltip;
import com.word.pc.word.Helperi.MySingleton;
import com.word.pc.word.Main;
import com.word.pc.word.Misc.RangCalculator;
import com.word.pc.word.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.word.pc.word.Main.avatar;

public class Qiuz1 extends AppCompatActivity {

    ProgressBar progressBar;
    TextView tvRangQ1, tvTrenutanScore, tvSledeciNivo, tvImeQ1,
            tvTrazenaRec, tvOpcija1, tvOpcija3, tvOpcija2, tvPitanje, tvVerzijaMesanja;
    Button btRefresh, btSubmit;
    LottieAnimationView Level5, Level4, Level3, Level2, Level1, trophy;
    ConstraintLayout quiz1_layout;
    int verzija, prebacenRangInt, prebacenScoreInt, granica_za_sledeci_nivo, prebacenHintInt;
    int[] verzions = {1, 2, 3, 4};
    String url_random = "http://lazyiguanastudios.com/webservices/Wordinary/randomWords.php";
    Animation animShake, animHorizontal;
    int brojacZaHintove = 0;
    Context context;
    ImageView imageView2, imageView3,ivFullScreen;
    int brojOkrenutekarte = 0;
    int hintIskoriscen = 0;
    private ConstraintSet layout1, layout2;
    private ConstraintLayout constraintLayout;
    private boolean isOpen = false;
    Random random;
    Typeface quick_bold,quick_light,quick_medium,quick_regular;
    String prebacenID;

    //----------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------- stringovi za test
    String opcija1;
    String opcija2;
    String opcija3;
    String tvTrazena;
    private String prebacenRang;
    private String prebacenScore;
    private String prebacenHint;
    private String randomWords;
    String[] opcije;
    private String tvTrazenaPrevod;

    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------

    public void init() {
        progressBar = findViewById(R.id.progressBar);
        tvRangQ1 = findViewById(R.id.tvRangQ1);
        tvTrenutanScore = findViewById(R.id.tvTrenutanScore);
        tvSledeciNivo = findViewById(R.id.tvSledeciNivo);
        tvImeQ1 = findViewById(R.id.tvImeQ1);
        tvTrazenaRec = findViewById(R.id.tvTrazenaRec);
        tvOpcija1 = findViewById(R.id.tvOpcija1);
        tvOpcija3 = findViewById(R.id.tvOpcija3);
        tvOpcija2 = findViewById(R.id.tvOpcija2);
        tvPitanje = findViewById(R.id.tvPitanje);
        btRefresh = findViewById(R.id.btRefresh);
        btSubmit = findViewById(R.id.btSubmit);
        Level1 = findViewById(R.id.Level1);
        Level2 = findViewById(R.id.Level2);
        Level3 = findViewById(R.id.Level3);
        Level4 = findViewById(R.id.Level4);
        Level5 = findViewById(R.id.Level5);
        trophy = findViewById(R.id.trophy);
        imageView2 = findViewById(R.id.imageView2);
        imageView3 = findViewById(R.id.imageView3);
        ivFullScreen = findViewById(R.id.ivFullScreen);
        quiz1_layout = findViewById(R.id.quiz1_layout);
        tvVerzijaMesanja = findViewById(R.id.tvVerzijaMesanja);
        animShake = AnimationUtils.loadAnimation(this, R.anim.shake);
        animHorizontal = AnimationUtils.loadAnimation(this, R.anim.shake_horizontal);
        context = this;
        prebacenID = getIntent().getStringExtra("id");
        //------------------------------------------------------------------------------------------ progres za progresBar
        prebacenRang = getIntent().getStringExtra("rang");
        prebacenScore = getIntent().getStringExtra("score");
        prebacenHint = getIntent().getStringExtra("hint");
        prebacenHintInt = Integer.parseInt(prebacenHint);
        prebacenRangInt = Integer.parseInt(prebacenRang);
        prebacenScoreInt = Integer.parseInt(prebacenScore);
        granica_za_sledeci_nivo = RangCalculator.setMaxScore(prebacenScoreInt);
        progressBar.setMax(granica_za_sledeci_nivo);
        progressBar.setProgress(prebacenScoreInt);
        tvSledeciNivo.setText("/" + String.valueOf(RangCalculator.setMaxScore(prebacenScoreInt)));
        tvTrenutanScore.setText(prebacenScore);
        tvRangQ1.setText(String.valueOf(prebacenRangInt));
        tvImeQ1.setText(prebacenHint.toString());
        btRefresh.setEnabled(false);
        btRefresh.setAlpha(.5f);
        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ layout animacija

        layout1 = new ConstraintSet();
        layout2 = new ConstraintSet();
        //constraintLayout = (ConstraintLayout) findViewById(R.id.quiz1_layout);
        layout2.clone(this, R.layout.quiz1_extended);
        layout1.clone(quiz1_layout);

        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------
        random = new Random();
       /* for (int i = 0; i < verzions.length; i++) {
            verzija = random.nextInt(verzions[i]);
        }*/
        tvVerzijaMesanja.setText(String.valueOf(verzija));

        Glide
                .with(context)
                .load(avatar)
                .apply(new RequestOptions().centerInside())
                .into(imageView2);

        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ fontovi
        quick_bold = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Bold.ttf");
        quick_light = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Light.ttf");
        quick_medium = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Medium.ttf");
        quick_regular = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Regular.ttf");

        tvPitanje.setTypeface(quick_medium);
        tvImeQ1.setTypeface(quick_medium);
        tvOpcija3.setTypeface(quick_medium);
        tvOpcija2.setTypeface(quick_medium);
        tvOpcija1.setTypeface(quick_medium);
        tvTrazenaRec.setTypeface(quick_medium);
        tvRangQ1.setTypeface(quick_medium);
        tvSledeciNivo.setTypeface(quick_medium);
        tvTrenutanScore.setTypeface(quick_medium);
        btRefresh.setTypeface(quick_medium);
        btSubmit.setTypeface(quick_medium);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qiuz1);
        getSupportActionBar().hide();
        init();
        randomWords();

        ArrayList stringoviRandom = new ArrayList();
        stringoviRandom.add(tvOpcija1.toString());
        stringoviRandom.add(tvOpcija2.toString());
        stringoviRandom.add(tvOpcija3.toString());
        stringoviRandom.add(tvTrazenaRec.toString());

        ivFullScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isOpen) {
                    TransitionManager.beginDelayedTransition(quiz1_layout);
                    layout2.applyTo(quiz1_layout);
                    isOpen = !isOpen;
                }else {
                    TransitionManager.beginDelayedTransition(quiz1_layout);
                    layout1.applyTo(quiz1_layout);
                    isOpen = !isOpen;
                }
            }
        });

        btRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < verzions.length; i++) {
                    verzija = random.nextInt(verzions[i]);
                }
                if (brojacZaHintove >9) {
                    brojacZaHintove = 0;
                    Level1.setProgress(0f);
                    Level2.setProgress(0f);
                    Level3.setProgress(0f);
                    Level4.setProgress(0f);
                    Level5.setProgress(0f);
                }
                tvVerzijaMesanja.setText(String.valueOf(verzija));
                randomWords();
                animShake.cancel();
                btSubmit.setEnabled(true);
                btSubmit.setAlpha(1f);
                btRefresh.setEnabled(false);
                btRefresh.setAlpha(.5f);
                tvTrazenaRec.setEnabled(true);
                tvOpcija1.setEnabled(true);
                tvOpcija2.setEnabled(true);
                tvOpcija3.setEnabled(true);
                tvTrazenaRec.setAlpha(1f);
                tvOpcija1.setAlpha(1f);
                tvOpcija2.setAlpha(1f);
                tvOpcija3.setAlpha(1f);
                tvOpcija1.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                tvOpcija2.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                tvOpcija3.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                tvTrazenaRec.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                //----------------------------------------------------------------------------------  otvaranje kartica
                final ObjectAnimator oa1 = ObjectAnimator.ofFloat(tvOpcija1, "scaleX", 0f, 1f);
                final ObjectAnimator oa2 = ObjectAnimator.ofFloat(tvOpcija2, "scaleX", 0f, 1f);
                final ObjectAnimator oa3 = ObjectAnimator.ofFloat(tvOpcija3, "scaleX", 0f, 1f);
                final ObjectAnimator oa4 = ObjectAnimator.ofFloat(tvTrazenaRec, "scaleX", 0f, 1f);
                oa1.start();
                oa2.start();
                oa3.start();
                oa4.start();
                imageView3.setEnabled(true);
                imageView3.setAlpha(1f);
                brojOkrenutekarte = 0;
            }
        });

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tvTrazenaRec.getBackground().getConstantState() == getResources().getDrawable(R.drawable.rv_avatar_background).getConstantState()) {

                    submitResult(tvTrazenaRec.getText().toString());
                } else if (tvOpcija1.getBackground().getConstantState() == getResources().getDrawable(R.drawable.rv_avatar_background).getConstantState()) {

                    submitResult(tvOpcija1.getText().toString());
                } else if (tvOpcija2.getBackground().getConstantState() == getResources().getDrawable(R.drawable.rv_avatar_background).getConstantState()) {

                    submitResult(tvOpcija2.getText().toString());
                } else if (tvOpcija3.getBackground().getConstantState() == getResources().getDrawable(R.drawable.rv_avatar_background).getConstantState()) {

                    submitResult(tvOpcija3.getText().toString());
                } else {
                    btSubmit.startAnimation(animHorizontal);
                }
            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvTrazenaRec.getAlpha() != 1f || tvOpcija1.getAlpha() != 1f || tvOpcija2.getAlpha() != 1f || tvOpcija3.getAlpha() != 1f) {
                    btRefresh.startAnimation(animShake);
                } else {
                    randomCoverWords(verzija);
                }
            }
        });


    }

    public void changeBack(View view) {
        int element_id = view.getId();
        switch (element_id) {
            case R.id.tvOpcija1:
                tvOpcija1.setBackground(getResources().getDrawable(R.drawable.rv_avatar_background));
                tvOpcija2.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                tvOpcija3.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                tvTrazenaRec.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                break;
            case R.id.tvOpcija2:
                tvOpcija1.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                tvOpcija2.setBackground(getResources().getDrawable(R.drawable.rv_avatar_background));
                tvOpcija3.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                tvTrazenaRec.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                break;
            case R.id.tvOpcija3:
                tvOpcija1.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                tvOpcija2.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                tvOpcija3.setBackground(getResources().getDrawable(R.drawable.rv_avatar_background));
                tvTrazenaRec.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                break;
            case R.id.tvTrazenaRec:
                tvOpcija1.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                tvOpcija2.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                tvOpcija3.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                tvTrazenaRec.setBackground(getResources().getDrawable(R.drawable.rv_avatar_background));
                break;

        }

    }

    private void randomWords() {

        StringRequest stringRequest2 = new StringRequest(Request.Method.POST, url_random, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                randomWords = response;
                opcije = randomWords.split("--");
                tvTrazena = opcije[0];
                tvTrazenaPrevod = opcije[1];
                opcija1 = opcije[3];
                opcija2 = opcije[5];
                opcija3 = opcije[7];

                tvPitanje.setText("Prevod reci " + tvTrazena.toUpperCase() + " glasi:");
                ranomize(verzija);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Нешто није у реду", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("english",etGermanS.getText().toString());
                params.put("user_id", prebacenID);
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest2);

    }

    public void ranomize(int version) {
        switch (version) {
            case 0:
                tvOpcija1.setText(opcija1);
                tvOpcija2.setText(opcija2);
                tvOpcija3.setText(opcija3);
                tvTrazenaRec.setText(tvTrazenaPrevod);
                break;
            case 1:
                tvOpcija1.setText(opcija3);
                tvOpcija2.setText(tvTrazenaPrevod);
                tvOpcija3.setText(opcija1);
                tvTrazenaRec.setText(opcija2);
                break;
            case 2:
                tvOpcija1.setText(tvTrazenaPrevod);
                tvOpcija2.setText(opcija1);
                tvOpcija3.setText(opcija2);
                tvTrazenaRec.setText(opcija3);
                break;
            case 3:
                tvOpcija1.setText(opcija2);
                tvOpcija2.setText(opcija1);
                tvOpcija3.setText(tvTrazenaPrevod);
                tvTrazenaRec.setText(opcija3);
                break;

        }
    }

    public void randomCoverWords(int version) {

        switch (version) {
            case 0:
                if (tvTrazenaRec.getAlpha() == .5f || tvOpcija1.getAlpha() == .5f || tvOpcija2.getAlpha() == .5f || tvOpcija3.getAlpha() == .5f) {
                    btRefresh.startAnimation(animShake);
                } else if (brojOkrenutekarte == 0) {
                    if (prebacenHintInt > 0) {
                        prebacenHintInt--;
                        tvImeQ1.setText(String.valueOf(prebacenHintInt));
                        final ObjectAnimator oa1 = ObjectAnimator.ofFloat(tvOpcija1, "scaleX", 1f, 0f);
                        oa1.setInterpolator(new DecelerateInterpolator());
                        oa1.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                brojOkrenutekarte++;
                                hintIskoriscen++;
                            }
                        });
                        oa1.start();
                    } else {
                        Toast.makeText(getApplicationContext(), "nema vise hintova", Toast.LENGTH_SHORT).show();
                    }
                } else if (brojOkrenutekarte == 1) {
                    if (prebacenHintInt > 0) {
                        prebacenHintInt--;
                        tvImeQ1.setText(String.valueOf(prebacenHintInt));
                        final ObjectAnimator oa1 = ObjectAnimator.ofFloat(tvOpcija3, "scaleX", 1f, 0f);
                        oa1.setInterpolator(new DecelerateInterpolator());
                        oa1.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                brojOkrenutekarte++;
                                hintIskoriscen++;
                            }
                        });
                        oa1.start();
                    } else {
                        Toast.makeText(getApplicationContext(), "nema vise hintova", Toast.LENGTH_SHORT).show();
                    }
                } else if (brojOkrenutekarte == 2) {
                    brojOkrenutekarte = 0;
                    imageView3.setEnabled(false);
                    imageView3.setAlpha(.5f);
                }
                break;
            case 1:
                if (brojOkrenutekarte == 0) {
                    if (prebacenHintInt > 0) {
                        prebacenHintInt--;
                        tvImeQ1.setText(String.valueOf(prebacenHintInt));
                        final ObjectAnimator oa1 = ObjectAnimator.ofFloat(tvTrazenaRec, "scaleX", 1f, 0f);
                        oa1.setInterpolator(new DecelerateInterpolator());
                        oa1.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                brojOkrenutekarte++;
                                hintIskoriscen++;
                            }
                        });
                        oa1.start();
                    } else {
                        Toast.makeText(getApplicationContext(), "nema vise hintova", Toast.LENGTH_SHORT).show();
                    }
                } else if (brojOkrenutekarte == 1) {
                    if (prebacenHintInt > 0) {
                        prebacenHintInt--;
                        tvImeQ1.setText(String.valueOf(prebacenHintInt));
                        final ObjectAnimator oa1 = ObjectAnimator.ofFloat(tvOpcija1, "scaleX", 1f, 0f);
                        oa1.setInterpolator(new DecelerateInterpolator());
                        oa1.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                brojOkrenutekarte++;
                                hintIskoriscen++;
                            }
                        });
                        oa1.start();
                    } else {
                        Toast.makeText(getApplicationContext(), "nema vise hintova", Toast.LENGTH_SHORT).show();
                    }
                } else if (brojOkrenutekarte == 2) {
                    brojOkrenutekarte = 0;
                    imageView3.setEnabled(false);
                    imageView3.setAlpha(.5f);
                }
                break;
            case 2:
                if (brojOkrenutekarte == 0) {
                    if (prebacenHintInt > 0) {
                        prebacenHintInt--;
                        tvImeQ1.setText(String.valueOf(prebacenHintInt));
                        final ObjectAnimator oa1 = ObjectAnimator.ofFloat(tvOpcija3, "scaleX", 1f, 0f);
                        oa1.setInterpolator(new DecelerateInterpolator());
                        oa1.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                brojOkrenutekarte++;
                                hintIskoriscen++;
                            }
                        });
                        oa1.start();
                    } else {
                        Toast.makeText(getApplicationContext(), "nema vise hintova", Toast.LENGTH_SHORT).show();
                    }
                } else if (brojOkrenutekarte == 1) {
                    if (prebacenHintInt > 0) {
                        prebacenHintInt--;
                        tvImeQ1.setText(String.valueOf(prebacenHintInt));
                        final ObjectAnimator oa1 = ObjectAnimator.ofFloat(tvOpcija2, "scaleX", 1f, 0f);
                        oa1.setInterpolator(new DecelerateInterpolator());
                        oa1.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                brojOkrenutekarte++;
                                hintIskoriscen++;
                            }
                        });
                        oa1.start();
                    } else {
                        Toast.makeText(getApplicationContext(), "nema vise hintova", Toast.LENGTH_SHORT).show();
                    }
                } else if (brojOkrenutekarte == 2) {
                    brojOkrenutekarte = 0;
                    imageView3.setEnabled(false);
                    imageView3.setAlpha(.5f);
                }
                break;
            case 3:
                if (brojOkrenutekarte == 0) {
                    if (prebacenHintInt > 0) {
                        prebacenHintInt--;
                        tvImeQ1.setText(String.valueOf(prebacenHintInt));
                        final ObjectAnimator oa1 = ObjectAnimator.ofFloat(tvTrazenaRec, "scaleX", 1f, 0f);
                        oa1.setInterpolator(new DecelerateInterpolator());
                        oa1.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                brojOkrenutekarte++;
                                hintIskoriscen++;
                            }
                        });
                        oa1.start();
                    } else {
                        Toast.makeText(getApplicationContext(), "nema vise hintova", Toast.LENGTH_SHORT).show();
                    }
                } else if (brojOkrenutekarte == 1) {
                    if (prebacenHintInt > 0) {
                        prebacenHintInt--;
                        tvImeQ1.setText(String.valueOf(prebacenHintInt));
                        final ObjectAnimator oa1 = ObjectAnimator.ofFloat(tvOpcija2, "scaleX", 1f, 0f);
                        oa1.setInterpolator(new DecelerateInterpolator());
                        oa1.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                brojOkrenutekarte++;
                                hintIskoriscen++;
                            }
                        });
                        oa1.start();
                    } else {
                        Toast.makeText(getApplicationContext(), "nema vise hintova", Toast.LENGTH_SHORT).show();
                    }
                } else if (brojOkrenutekarte == 2) {
                    brojOkrenutekarte = 0;
                    imageView3.setEnabled(false);
                    imageView3.setAlpha(.5f);
                }
                break;
        }

    }

    public void submitResult(String answer) {

        if (answer.toString().equals(tvTrazenaPrevod) && hintIskoriscen == 0) {
            tvTrazenaRec.setEnabled(false);
            tvOpcija1.setEnabled(false);
            tvOpcija2.setEnabled(false);
            tvOpcija3.setEnabled(false);
            tvTrazenaRec.setAlpha(.5f);
            tvOpcija1.setAlpha(.5f);
            tvOpcija2.setAlpha(.5f);
            tvOpcija3.setAlpha(.5f);
            btRefresh.startAnimation(animShake);
            btSubmit.setEnabled(false);
            btSubmit.setAlpha(.5f);
            btRefresh.setEnabled(true);
            btRefresh.setAlpha(1f);

            prebacenScoreInt++;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                progressBar.setProgress(prebacenScoreInt, true);
            }
            tvTrenutanScore.setText(String.valueOf(prebacenScoreInt));
            if (prebacenScoreInt > granica_za_sledeci_nivo) {

                //----------------------------------------------------------------------------------

                int cx = trophy.getWidth() / 2;
                int cy = trophy.getHeight() / 2;

                // get the final radius for the clipping circle
                float finalRadius = (float) Math.hypot(cx, cy);

                // create the animator for this view (the start radius is zero)
                Animator anim =
                        ViewAnimationUtils.createCircularReveal(trophy, cx, cy, 0, finalRadius);

                // make the view visible and start the animation
                trophy.setVisibility(View.VISIBLE);

                anim.start();
                trophy.playAnimation();
                trophy.addAnimatorListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        int cx = trophy.getWidth() / 2;
                        int cy = trophy.getHeight() / 2;

                        float initialRadius = (float) Math.hypot(cx, cy);

                        Animator anim =
                                ViewAnimationUtils.createCircularReveal(trophy, cx, cy, initialRadius, 0);

                        anim.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);

                                trophy.setVisibility(View.INVISIBLE);

                            }
                        });

                        anim.start();
                    }
                });


                //---------------------------------------------------------------------------------- todo trofej json animacija
                tvSledeciNivo.setText("/" + String.valueOf(RangCalculator.setMaxScore(prebacenScoreInt)));
                prebacenRangInt++;
                tvRangQ1.setText(String.valueOf(prebacenRangInt));
                granica_za_sledeci_nivo = RangCalculator.setMaxScore(prebacenScoreInt);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    progressBar.setProgress(prebacenScoreInt, true);
                }
                progressBar.setMax(granica_za_sledeci_nivo);
            }

            //---------------------------------------------------------------------------------- hintovi!!
            brojacZaHintove++;
            //Toast.makeText(getApplicationContext(),"brojzahintove je: " + brojacZaHintove,Toast.LENGTH_SHORT);
            switch (brojacZaHintove) {
                case 2:
                    Level1.playAnimation();
                    break;
                case 4:
                    Level2.playAnimation();
                    break;
                case 6:
                    Level2.playAnimation();
                    Level3.playAnimation();
                    break;
                case 8:
                    Level3.playAnimation();
                    Level4.playAnimation();
                    break;
                case 9:
                    Level1.playAnimation();
                    Level2.playAnimation();
                    Level3.playAnimation();
                    Level4.playAnimation();
                    Tooltip tooltip = new Tooltip.Builder(Level4)
                            .setText("jos jedan i hint!")
                            .setTextColor(Color.WHITE)
                            .setGravity(Gravity.TOP)
                            .setCornerRadius(8f)
                            .setDismissOnClick(true)
                            .setCancelable(true)
                            .setBackgroundColor(getResources().getColor(R.color.zuta))
                            .show();
                    break;
                case 10:
                    Level1.setRepeatCount(1);
                    Level1.playAnimation();
                    Level2.setRepeatCount(1);
                    Level2.playAnimation();
                    Level3.setRepeatCount(1);
                    Level3.playAnimation();
                    Level4.setRepeatCount(1);
                    Level4.playAnimation();
                    Level5.setRepeatCount(1);
                    Level5.playAnimation();
                    prebacenHintInt++;
                    tvImeQ1.setText(String.valueOf(prebacenHintInt));
                    Level5.addAnimatorListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            Level1.setProgress(0f);
                            Level2.setProgress(0f);
                            Level3.setProgress(0f);
                            Level4.setProgress(0f);
                            Level5.setProgress(0f);
                        }
                    });

                    break;
            }

        } else if (answer.toString().equals(tvTrazenaPrevod) && hintIskoriscen > 0) {
            tvTrazenaRec.setEnabled(false);
            tvOpcija1.setEnabled(false);
            tvOpcija2.setEnabled(false);
            tvOpcija3.setEnabled(false);
            tvTrazenaRec.setAlpha(.5f);
            tvOpcija1.setAlpha(.5f);
            tvOpcija2.setAlpha(.5f);
            tvOpcija3.setAlpha(.5f);
            btRefresh.startAnimation(animShake);
            brojacZaHintove = 0;
            hintIskoriscen = 0;
            Level1.setProgress(0f);
            Level2.setProgress(0f);
            Level3.setProgress(0f);
            Level4.setProgress(0f);
            Level5.setProgress(0f);
            btSubmit.setEnabled(false);
            btSubmit.setAlpha(.5f);
            btRefresh.setEnabled(true);
            btRefresh.setAlpha(1f);
            Toast.makeText(getApplicationContext(), "tacno,nema poena,brise se level", Toast.LENGTH_SHORT).show();

        } else {
            brojOkrenutekarte = 0;
            imageView3.setEnabled(false);
            imageView3.setAlpha(.5f);
            brojacZaHintove = 0;
            tvTrazenaRec.setEnabled(false);
            tvOpcija1.setEnabled(false);
            tvOpcija2.setEnabled(false);
            tvOpcija3.setEnabled(false);
            Level1.setProgress(0f);
            Level2.setProgress(0f);
            Level3.setProgress(0f);
            Level4.setProgress(0f);
            Level5.setProgress(0f);
            Toast.makeText(getApplicationContext(), "netacno", Toast.LENGTH_SHORT).show();
            btRefresh.startAnimation(animShake);
            btSubmit.setEnabled(false);
            btSubmit.setAlpha(.5f);
            btRefresh.setEnabled(true);
            btRefresh.setAlpha(1f);
        }
    }


}

package com.word.pc.word.Adapteri;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.word.pc.word.Misc.DbContract;
import com.word.pc.word.Modeli.Word;
import com.word.pc.word.R;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

    private ArrayList<Word> arrayList;
    private Context context;
    private int lastPosition = -1;

    public RecyclerAdapter(ArrayList<Word> arrayList,Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tvRec.setText(arrayList.get(position).getWord());
        holder.tvPevod.setText(arrayList.get(position).getTranslate());

        holder.tvPrevodNaslov.setTypeface(holder.quick_regular);
        holder.tvRecNaslov.setTypeface(holder.quick_regular);
        holder.tvRec.setTypeface(holder.quick_medium);
        holder.tvPevod.setTypeface(holder.quick_medium);
        holder.btDelete.setTypeface(holder.quick_medium);
        holder.btEdit.setTypeface(holder.quick_medium);


        int sync_status = arrayList.get(position).getSync_status();
        if(sync_status == DbContract.SYNC_STATUS_OK)
        {
            //holder.Sync_status.setImageResource(R.drawable.ic_menu_send);
            holder.Sync_status.setVisibility(View.INVISIBLE);
            holder.rv_item.setAlpha(1f);
            holder.rv_item.setEnabled(true);
        }else
        {
            holder.Sync_status.setImageResource(R.drawable.ic_refresh_black_18dp);
            holder.Sync_status.setVisibility(View.VISIBLE);
            holder.rv_item.setAlpha(.5f);
            holder.rv_item.setEnabled(false);
        }

        setAnimation(holder.itemView, position);

        holder.btEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"edit klik" + position +
                        " "+holder.tvPevod.getText().toString() + " " + holder.tvRec.getText().toString(),Toast.LENGTH_SHORT).show();

                //---------------------------------------------------------------------------------- todo odradi klikove!!
            }
        });

        holder.btDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"delete klik" + position +
                        " "+holder.tvPevod.getText().toString() + " " + holder.tvRec.getText().toString(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {

        ImageView Sync_status;
        TextView tvRec,tvPevod,tvRecNaslov,tvPrevodNaslov;
        Button btEdit,btDelete;
        ConstraintLayout rv_item;
        Typeface quick_bold,quick_light,quick_medium,quick_regular;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvRec = itemView.findViewById(R.id.tvRec);
            tvPevod = itemView.findViewById(R.id.tvPevod);
            tvRecNaslov = itemView.findViewById(R.id.tvRecNaslov);
            tvPrevodNaslov = itemView.findViewById(R.id.tvPrevodNaslov);
            btEdit= itemView.findViewById(R.id.btEdit);
            btDelete= itemView.findViewById(R.id.btDelete);
            Sync_status = itemView.findViewById(R.id.ivSync);
            rv_item = itemView.findViewById(R.id.rv_item2);
            //-----------------------------------------------
            quick_bold = Typeface.createFromAsset(context.getAssets(),"fonts/Quicksand-Bold.ttf");
            quick_light = Typeface.createFromAsset(context.getAssets(),"fonts/Quicksand-Light.ttf");
            quick_medium = Typeface.createFromAsset(context.getAssets(),"fonts/Quicksand-Medium.ttf");
            quick_regular = Typeface.createFromAsset(context.getAssets(),"fonts/Quicksand-Regular.ttf");
        }
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    //----------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------- za search bar

    public void setFilter(ArrayList<Word>newList){
        arrayList = new ArrayList<>();
        arrayList.addAll(newList);
        notifyDataSetChanged();
    }
}
package com.word.pc.word.Modeli;

public class User {
    private String username;
    private String lastname;
    private int score;
    private int avatar;
    private int id;

    public User(String username, String lastname, int score, int avatar,int id) {
        this.username = username;
        this.lastname = lastname;
        this.score = score;
        this.avatar = avatar;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return username;
    }

    public void setName(String username) {
        this.username = username;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }
}

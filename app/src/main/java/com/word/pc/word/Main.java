package com.word.pc.word;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.tooltip.Tooltip;
import com.word.pc.word.Adapteri.RecyclerAdapter;
import com.word.pc.word.Aktivnosti.LeaderBoard;
import com.word.pc.word.Aktivnosti.Proile;
import com.word.pc.word.Aktivnosti.Qiuz1;
import com.word.pc.word.Aktivnosti.Qiuz2;
import com.word.pc.word.Helperi.DbHelper;
import com.word.pc.word.Helperi.MySingleton;
import com.word.pc.word.Misc.DbContract;
import com.word.pc.word.Misc.Prefs;
import com.word.pc.word.Misc.RangCalculator;
import com.word.pc.word.Misc.Utils;
import com.word.pc.word.Modeli.Word;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener{

    RecyclerView rvWords;
    EditText etWord, etTranslate;
    Button btSubmit;
    RecyclerView.LayoutManager layoutManager;
    RecyclerAdapter adapter;
    ArrayList<Word> arrayList = new ArrayList<>();
    private ConstraintSet layout1, layout2;
    private ConstraintLayout constraintLayout;
    private boolean isOpen = false;
    String url_send = "http://lazyiguanastudios.com/webservices/Wordinary/connection.php";
    String url_details = "http://lazyiguanastudios.com/webservices/Wordinary/userDeatils.php";
    FloatingActionButton fab, fab2;
    Switch nightMode;
    String word,translate,prebacenID,resultScore;
    ImageView ivProfile, ivBadge;
    View view2;
    boolean isFliped = false;
    RelativeLayout relativeLayout;
    String resultIme, resultPrezime, resultRang, resultAvatar,resultHint,resultUsername;
    int rang, score,brojReci,prebacenIDint;
    Context context;
    public static Drawable avatar, badge;
    TextView tvName, tvRang,tvOfflineWords;
    Animation animHorizontal;
    Typeface quick_bold,quick_light,quick_medium,quick_regular;


    public void init() {
        rvWords = findViewById(R.id.rvWords);
        etWord = findViewById(R.id.etWord);
        etTranslate = findViewById(R.id.etTranslate);
        btSubmit = findViewById(R.id.btSubmit);
        layoutManager = new LinearLayoutManager(this);
        rvWords.setLayoutManager(layoutManager);
        rvWords.setHasFixedSize(true);
        adapter = new RecyclerAdapter(arrayList, this);
        rvWords.setAdapter(adapter);
        view2 = findViewById(R.id.view2);
        context = this;
        animHorizontal = AnimationUtils.loadAnimation(this, R.anim.shake_horizontal);
        prebacenID = getIntent().getStringExtra("id");
        prebacenIDint = Integer.parseInt(prebacenID);

        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ layout animacija

        layout1 = new ConstraintSet();
        layout2 = new ConstraintSet();
        constraintLayout = (ConstraintLayout) findViewById(R.id.constraint_layout);
        layout2.clone(this, R.layout.content_main_extended);
        layout1.clone(constraintLayout);

        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ fontovi
        quick_bold = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Bold.ttf");
        quick_light = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Light.ttf");
        quick_medium = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Medium.ttf");
        quick_regular = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Regular.ttf");

        etTranslate.setTypeface(quick_regular);
        etWord.setTypeface(quick_regular);
        btSubmit.setTypeface(quick_medium);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        init();
        readFromLocalStorage(prebacenIDint);
        userDetails();
        Utils.onActivityCreateSetTheme(this);


        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ fab/fab2 circular reveal

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab2 = (FloatingActionButton) findViewById(R.id.fab2);

        fab.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                if (!isOpen) {
                    TransitionManager.beginDelayedTransition(constraintLayout);
                    layout2.applyTo(constraintLayout);
                    isOpen = !isOpen;
                    fab2.animate().rotation(360f).setDuration(500);

                    // Check if the runtime version is at least Lollipop
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        // get the center for the clipping circle
                        int cx = fab2.getWidth() / 2;
                        int cy = fab2.getHeight() / 2;

                        // get the final radius for the clipping circle
                        float finalRadius = (float) Math.hypot(cx, cy);

                        // create the animator for this view (the start radius is zero)
                        Animator anim =
                                ViewAnimationUtils.createCircularReveal(fab2, cx, cy, 0, finalRadius);

                        // make the view visible and start the animation
                        fab2.setVisibility(View.VISIBLE);

                        anim.start();
                        fab.setEnabled(false);


                    } else {
                        // set the view to visible without a circular reveal animation below Lollipop
                        fab2.setVisibility(View.VISIBLE);
                        fab.setEnabled(false);
                    }


                } else {
                    TransitionManager.beginDelayedTransition(constraintLayout);
                    layout1.applyTo(constraintLayout);
                    isOpen = !isOpen;
                    fab2.animate().rotation(360f).setDuration(500);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        // get the center for the clipping circle
                        int cx = fab2.getWidth() / 2;
                        int cy = fab2.getHeight() / 2;

                        // get the final radius for the clipping circle
                        float finalRadius = (float) Math.hypot(cx, cy);

                        // create the animator for this view (the start radius is zero)
                        Animator anim =
                                ViewAnimationUtils.createCircularReveal(fab2, cx, cy, 0, finalRadius);

                        // make the view visible and start the animation
                        fab2.setVisibility(View.VISIBLE);
                        anim.start();
                        fab.setEnabled(false);
                    } else {
                        // set the view to visible without a circular reveal animation below Lollipop
                        fab2.setVisibility(View.VISIBLE);
                        fab.setEnabled(false);
                    }
                }
            }
        });

        fab2.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                if (!isOpen) {
                    TransitionManager.beginDelayedTransition(constraintLayout);
                    layout2.applyTo(constraintLayout);
                    isOpen = !isOpen;
                    fab2.animate().rotation(-360f).setDuration(500);
                    // Check if the runtime version is at least Lollipop
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        int cx = fab2.getWidth() / 2;
                        int cy = fab2.getHeight() / 2;

                        float initialRadius = (float) Math.hypot(cx, cy);

                        Animator anim =
                                ViewAnimationUtils.createCircularReveal(fab2, cx, cy, initialRadius, 0);

                        anim.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);

                                fab2.setVisibility(View.INVISIBLE);
                                fab.setEnabled(true);
                            }
                        });

                        anim.start();
                    } else {
                        // set the view to visible without a circular reveal animation below Lollipop
                        fab2.setVisibility(View.INVISIBLE);
                        fab.setEnabled(true);
                    }


                } else {
                    TransitionManager.beginDelayedTransition(constraintLayout);
                    layout1.applyTo(constraintLayout);
                    isOpen = !isOpen;
                    fab2.animate().rotation(-360f).setDuration(500);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        int cx = fab2.getWidth() / 2;
                        int cy = fab2.getHeight() / 2;

                        float initialRadius = (float) Math.hypot(cx, cy);

                        Animator anim =
                                ViewAnimationUtils.createCircularReveal(fab2, cx, cy, initialRadius, 0);

                        anim.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                fab2.setVisibility(View.INVISIBLE);
                                fab.setEnabled(true);
                            }
                        });

                        anim.start();
                    } else {
                        // set the view to visible without a circular reveal animation below Lollipop
                        fab2.setVisibility(View.INVISIBLE);
                        fab.setEnabled(true);
                    }
                }
            }
        });

        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        tvOfflineWords=(TextView) MenuItemCompat.getActionView(navigationView.getMenu().
                findItem(R.id.nav_send));
        getOfflineWords();
        nightMode = (Switch) MenuItemCompat.getActionView(navigationView.getMenu().
                findItem(R.id.nav_settings));
        //-----------------------------------------promena teme

        View headerview = navigationView.getHeaderView(0);
        tvName = headerview.findViewById(R.id.tvName);
        tvRang = headerview.findViewById(R.id.tvRang);
        ivProfile = (ImageView) headerview.findViewById(R.id.ivProfile);
        ivBadge = (ImageView) headerview.findViewById(R.id.ivBadge);
        relativeLayout = headerview.findViewById(R.id.relativeLayout);

        tvName.setTypeface(quick_light);
        tvRang.setTypeface(quick_light);
        tvOfflineWords.setTypeface(quick_bold);

        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tooltip tooltip = new Tooltip.Builder(ivProfile)
                        .setText("Swipe down to Login!")
                        .setTextColor(Color.WHITE)
                        .setGravity(Gravity.BOTTOM)
                        .setCornerRadius(8f)
                        .setDismissOnClick(true)
                        .setCancelable(true)
                        .setBackgroundColor(getResources().getColor(R.color.zuta))
                        .show();
            }
        });


        //------------------------------------------------------------------------------------------ FLIP!

        tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ObjectAnimator oa1 = ObjectAnimator.ofFloat(relativeLayout, "scaleX", 1f, 0f);
                final ObjectAnimator oa2 = ObjectAnimator.ofFloat(relativeLayout, "scaleX", 0f, 1f);
                oa1.setInterpolator(new DecelerateInterpolator());
                oa2.setInterpolator(new AccelerateDecelerateInterpolator());
                oa1.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);

                        if (isFliped == false) {
                            relativeLayout.setBackground(getResources().getDrawable(R.drawable.placeholder_yelow));
                            ivProfile.setVisibility(View.INVISIBLE);
                            tvRang.setVisibility(View.VISIBLE);
                            oa2.start();
                            isFliped = true;
                        } else {
                            relativeLayout.setBackground(getResources().getDrawable(R.drawable.placeholder));
                            ivProfile.setVisibility(View.VISIBLE);
                            tvRang.setVisibility(View.INVISIBLE);
                            oa2.start();
                            isFliped = false;
                        }
                    }
                });
                oa1.start();
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        //------------------------------------------------------------------------------------------ search bar
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        //tvOfflineWords = findViewById(R.id.textView4);
        //tvOfflineWords.setText(String.valueOf(brojReci));

        if (id == R.id.nav_camera) {
            Intent intent = new Intent(getApplicationContext(), Proile.class);
            intent.putExtra("name", resultIme);
            intent.putExtra("lastname", resultPrezime);
            intent.putExtra("rang", resultRang);
            intent.putExtra("score", resultScore);
            intent.putExtra("avatar", resultAvatar);
            startActivity(intent);

        } else if (id == R.id.nav_gallery) {
           /*if(tvRang.getText().toString().equals("Librarian")){
               Intent intent = new Intent(getApplicationContext(),Qiuz1.class);
               intent.putExtra("name", resultIme);
               intent.putExtra("lastname", resultPrezime);
               intent.putExtra("rang", resultRang);
               intent.putExtra("score", resultScore);
               intent.putExtra("hint", resultHint);
               intent.putExtra("avatar", resultAvatar);
               intent.putExtra("id", prebacenID);
               startActivity(intent);
           }else {
               Toast.makeText(getApplicationContext(),"You must be at least Librarian!",Toast.LENGTH_LONG).show();

           }*/

            Intent intent = new Intent(getApplicationContext(),Qiuz1.class);
            intent.putExtra("name", resultIme);
            intent.putExtra("lastname", resultPrezime);
            intent.putExtra("rang", resultRang);
            intent.putExtra("score", resultScore);
            intent.putExtra("hint", resultHint);
            intent.putExtra("avatar", resultAvatar);
            intent.putExtra("id", prebacenID);
            startActivity(intent);

        } else if (id == R.id.nav_slideshow) {
            /*if(tvRang.getText().toString().equals("Attendant")) {
                Intent intent = new Intent(getApplicationContext(), Qiuz2.class);
                intent.putExtra("name", resultIme);
                intent.putExtra("lastname", resultPrezime);
                intent.putExtra("rang", resultRang);
                intent.putExtra("score", resultScore);
                intent.putExtra("hint", resultHint);
                intent.putExtra("avatar", resultAvatar);
                intent.putExtra("id", prebacenID);
                startActivity(intent);
            }else {
                Toast.makeText(getApplicationContext(),"You must be at least Attendant",Toast.LENGTH_SHORT).show();
            }*/
            Intent intent = new Intent(getApplicationContext(), Qiuz2.class);
            intent.putExtra("name", resultIme);
            intent.putExtra("lastname", resultPrezime);
            intent.putExtra("rang", resultRang);
            intent.putExtra("score", resultScore);
            intent.putExtra("hint", resultHint);
            intent.putExtra("avatar", resultAvatar);
            intent.putExtra("id", prebacenID);
            startActivity(intent);

        } else if (id == R.id.nav_share) {
            Intent intent = new Intent(getApplicationContext(),LeaderBoard.class);
            intent.putExtra("username", resultUsername);
            intent.putExtra("name", resultIme);
            intent.putExtra("lastname", resultPrezime);
            intent.putExtra("rang", resultRang);
            intent.putExtra("score", resultScore);
            intent.putExtra("hint", resultHint);
            intent.putExtra("avatar", resultAvatar);
            intent.putExtra("id", prebacenID);
            startActivity(intent);

        } else if (id == R.id.nav_send) {
            if (checknetworkConncetion()) {
                word = etWord.getText().toString();
                translate = etTranslate.getText().toString();
                final DbHelper dbHelper = new DbHelper(getApplicationContext());
                final SQLiteDatabase database = dbHelper.getWritableDatabase();

                //Cursor cursor = dbHelper.readFromLocalDatbase(database);
                Cursor cursor = database.rawQuery("SELECT * FROM words WHERE user_id ="+ prebacenIDint,null);//todo selekcija reci po user_id

                while (cursor.moveToNext()) {
                    int sync_status = cursor.getInt(cursor.getColumnIndex(DbContract.SYNC_STATUS));
                    if (sync_status == DbContract.SYNC_STATUS_FAILED) {
                        final String Word = cursor.getString(cursor.getColumnIndex(DbContract.WORD));
                        final String Translate = cursor.getString(cursor.getColumnIndex(DbContract.TRANSLATE));

                        StringRequest stringRequest3 = new StringRequest(Request.Method.POST, url_send, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {

                                    JSONObject jsonObject = new JSONObject(response);
                                    String Response = jsonObject.getString("response");
                                    if (Response.equals("OK")) {
                                        dbHelper.updateLocalDatabase(Word, Translate, prebacenIDint, DbContract.SYNC_STATUS_OK, database);
                                        readFromLocalStorage(prebacenIDint);
                                        getOfflineWords();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                        getOfflineWords();

                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("word", Word);
                                params.put("translate", Translate);
                                params.put("user_id", prebacenID);
                                return params;
                            }
                        };
                        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest3);
                    }
                }

                //readFromLocalStorage();
            }
        } else if (id == R.id.nav_settings) {


        } else if (id == R.id.nav_exit) {
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------

    private void saveToAppServer(final String Word, final String Translate) {

        if (checknetworkConncetion()) {
            //--------------------------------------------------------------------------------------todo ubaci ovo u metodu!!
            StringRequest stringRequest3 = new StringRequest(Request.Method.POST, url_send, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String Response = jsonObject.getString("response");
                        if (Response.equals("OK")) {
                            saveToLocalStorage(Word, Translate, prebacenIDint, DbContract.SYNC_STATUS_OK);
                            getOfflineWords();

                        } else {
                            saveToLocalStorage(Word, Translate, prebacenIDint, DbContract.SYNC_STATUS_FAILED);
                            getOfflineWords();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    saveToLocalStorage(Word, Translate, prebacenIDint, DbContract.SYNC_STATUS_FAILED);
                    getOfflineWords();

                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("word", Word);
                    params.put("translate", Translate);
                    params.put("user_id", prebacenID);
                    return params;
                }
            };
            MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest3);

        } else {
            saveToLocalStorage(word, translate, prebacenIDint, DbContract.SYNC_STATUS_FAILED);
            getOfflineWords();
        }


    }

    //----------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------- cuvanje u SQLite

    private void saveToLocalStorage(String word, String translate, int user_id, int sync_status) {
        DbHelper dbHelper = new DbHelper(this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        dbHelper.SaveToLocalDatabase(word, translate, user_id, sync_status, database);
        readFromLocalStorage(prebacenIDint);
        adapter.notifyDataSetChanged();

    }

    //----------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------- slanje reci

    public void submitWord(View view) {


        if (etWord.getText().toString().equals("") || etTranslate.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "oba polja obavezna", Toast.LENGTH_SHORT).show();
            //btSubmit.startAnimation(animHorizontal);
        } else {
            word = etWord.getText().toString();
            translate = etTranslate.getText().toString();
            saveToAppServer(word, translate);
            //saveToLocalStorage(word, translate, 1, 0);

            //---------------------------------------------------------------------------------------------------------------

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // get the center for the clipping circle
                int cx = view2.getWidth() / 2;
                int cy = view2.getHeight() / 2;

                // get the final radius for the clipping circle
                float finalRadius = (float) Math.hypot(cx, cy);

                // create the animator for this view (the start radius is zero)
                Animator anim =
                        ViewAnimationUtils.createCircularReveal(view2, cx, cy, 0, finalRadius);

                // make the view visible and start the animation
                view2.setVisibility(View.VISIBLE);
                anim.start();
                anim.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        // view2.setVisibility(View.INVISIBLE);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            int cx = view2.getWidth() / 2;
                            int cy = view2.getHeight() / 2;

                            float initialRadius = (float) Math.hypot(cx, cy);

                            Animator anim =
                                    ViewAnimationUtils.createCircularReveal(view2, cx, cy, initialRadius, 0);

                            anim.addListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    view2.setVisibility(View.INVISIBLE);

                                }
                            });

                            anim.start();
                        } else {
                            // set the view to visible without a circular reveal animation below Lollipop
                            view2.setVisibility(View.INVISIBLE);
                        }


                        super.onAnimationEnd(animation);
                    }
                });

            }

            //---------------------------------------------------------------------------------------------------------------

            etWord.setText("");
            etTranslate.setText("");
        }

    }

    //----------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------- citanje iz SQLite
    private void readFromLocalStorage(int userID) {
        this.prebacenIDint = userID;
        arrayList.clear();
        DbHelper dbHelper = new DbHelper(this);
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        //Cursor cursor = dbHelper.readFromLocalDatbase(database);
        Cursor cursor = database.rawQuery("SELECT * FROM words WHERE user_id ="+ userID,null);//todo selekcija reci po user_id

        while (cursor.moveToNext()) {
            String name = cursor.getString(cursor.getColumnIndex(DbContract.WORD));
            String translate = cursor.getString(cursor.getColumnIndex(DbContract.TRANSLATE));
            int sync_status = cursor.getInt(cursor.getColumnIndex(DbContract.SYNC_STATUS));
            int user_id = cursor.getInt(cursor.getColumnIndex(DbContract.USER_ID));
            arrayList.add(new Word(name, translate, sync_status, user_id));
        }

        adapter.notifyDataSetChanged();
        //cursor.close();

    }

    //----------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------- provera internet konekcije
    public boolean checknetworkConncetion() {
        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    //----------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------- search bar

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        newText = newText.toLowerCase();
        ArrayList<Word> newList = new ArrayList<>();
        for (Word word : arrayList) {
            String Word = word.getWord().toString();
            if (Word.contains(newText)) {
                newList.add(word);
            }

        }
        adapter.setFilter(newList);
        return true;
    }
    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------

    private void userDetails() {

        StringRequest stringRequest2 = new StringRequest(Request.Method.POST, url_details, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response.toString());

                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    JSONObject jsonRow = jsonArray.getJSONObject(0);

                    resultUsername = jsonRow.getString("username");
                    resultIme = jsonRow.getString("name");
                    resultPrezime = jsonRow.getString("lastname");
                    resultRang = jsonRow.getString("rang");
                    rang = Integer.parseInt(resultRang);
                    resultScore = jsonRow.getString("score");
                    score = Integer.parseInt(resultRang);
                    resultAvatar = jsonRow.getString("avatar");
                    resultHint = jsonRow.getString("hint");
                    String pocetakA = "avatars/";
                    String pocetakB = "badges/";
                    String slika = pocetakA.concat(resultAvatar).concat(".png");
                    String slikaBadge = pocetakB.concat(resultRang).concat(".png");

                    try {
                        avatar = Drawable.createFromStream(context.getAssets().open(slika), null);
                        badge = Drawable.createFromStream(context.getAssets().open(slikaBadge), null);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    Glide
                            .with(context)
                            .load(avatar)
                            //.apply( new RequestOptions().placeholder(R.drawable.placeholder))
                            .into(ivProfile);

                    Glide
                            .with(context)
                            .load(badge)
                            //.apply( new RequestOptions().placeholder(R.drawable.placeholder))
                            .into(ivBadge);

                    tvName.setText(resultIme + " " + resultPrezime);
                    tvRang.setText("rang:\n" + RangCalculator.setRangName(rang));


                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Нешто није у реду", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("english",etGermanS.getText().toString());
                params.put("id", prebacenID);
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest2);

    }

    public void getOfflineWords(){
        DbHelper dbHelper = new DbHelper(this);
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        //brojReci = dbHelper.readWordNotOnServer(database);//------------------------------------------- broj reci koje nisu upload-ovane
        //------------------------------------------------------------------------------------------
        int nombr = 0;
        Cursor cursor = database.rawQuery("SELECT word FROM words WHERE syncstatus = 1 AND user_id ="+ prebacenIDint, null);
        brojReci = cursor.getCount();
        //------------------------------------------------------------------------------------------
        if(brojReci>0 && brojReci<=10) {
            tvOfflineWords.setVisibility(View.VISIBLE);
            tvOfflineWords.setGravity(Gravity.CENTER_VERTICAL);
            tvOfflineWords.setTypeface(null, Typeface.BOLD);
            tvOfflineWords.setTextColor(getResources().getColor(R.color.colorAccent));
            tvOfflineWords.setText(String.valueOf(brojReci));
        }else if(brojReci>10){
            tvOfflineWords.setVisibility(View.VISIBLE);
            tvOfflineWords.setGravity(Gravity.CENTER_VERTICAL);
            tvOfflineWords.setTypeface(null, Typeface.BOLD);
            tvOfflineWords.setTextColor(getResources().getColor(R.color.colorAccent));
            tvOfflineWords.setText("10+");
        }else{
            tvOfflineWords.setVisibility(View.INVISIBLE);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    public void changeTheme(int version){
        RangCalculator.THEME_VER = version;
        if(RangCalculator.THEME_VER == 0){
            setTheme(R.style.AppTheme);
        }else {
            setTheme(R.style.AppTheme2);
        }
    }



    // todo odradi delete
    // todo odradi cisto svlacenje sa servera kada je sqlite prazan
    // todo odradi slanje i user_id uz reci
}

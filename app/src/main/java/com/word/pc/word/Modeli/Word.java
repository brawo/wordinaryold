package com.word.pc.word.Modeli;

public class Word {

    private String word;
    private String translate;
    private int sync_status;
    private int user_id;

    public Word(String word, String translate, int sync_status, int user_id) {
        this.word = word;
        this.translate = translate;
        this.sync_status = sync_status;
        this.user_id = user_id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getTranslate() {
        return translate;
    }

    public void setTranslate(String translate) {
        this.translate = translate;
    }

    public int getSync_status() {
        return sync_status;
    }

    public void setSync_status(int sync_status) {
        this.sync_status = sync_status;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}

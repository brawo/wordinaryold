package com.word.pc.word.Aktivnosti;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionManager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.word.pc.word.Adapteri.RecyclerAvatar;
import com.word.pc.word.Adapteri.ViewPagerAdapter;
import com.word.pc.word.Helperi.MySingleton;
import com.word.pc.word.Misc.RangCalculator;
import com.word.pc.word.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.word.pc.word.Misc.RangCalculator.slike;

public class Proile extends AppCompatActivity {
    RecyclerView rvAvatar;
    RecyclerView.LayoutManager layoutManager;
    RecyclerAvatar adapter;
    TextView tvFullName, tvScore, tvTrophy;
    ImageButton ibScore, ibTrophy, ibArrow;
    public static ImageView ivProfileP, ivBadgeP;
    private ConstraintSet layout1, layout2;
    private ConstraintLayout constraintLayout;
    private boolean isOpen = false;
    private Drawable avatar,badge;
    public static int prebacenAvatarInt;
    Context context;
    Typeface quick_bold,quick_light,quick_medium,quick_regular;

    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------

    String prebacenoIme,prebacenoPrezime,prebacenRang,prebacenScore,prebacenAvatar;
    int prebacenRangInt,prebacenScoreInt;

    //----------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------- prebaceno iz Main

    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    String[] avatari = {"1", "2", "3", "4","5","6","7","9","10",
            "11", "12","13","14","15","17","18","19", "20", "21"};

    public void init() {

        //----------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------
        prebacenoIme = getIntent().getStringExtra("name");
        prebacenoPrezime = getIntent().getStringExtra("lastname");
        prebacenRang = getIntent().getStringExtra("rang");
        prebacenScore = getIntent().getStringExtra("score");
        prebacenAvatar = getIntent().getStringExtra("avatar");

        prebacenRangInt = Integer.parseInt(prebacenRang);
        prebacenAvatarInt= Integer.parseInt(prebacenAvatar);

        //----------------------------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------

        ArrayList list = new ArrayList();
        for(int i = 0;i<=prebacenRangInt*5;i++){
            String avatariFinal = avatari[i];
            list.addAll(Arrays.asList(avatariFinal));

        }
        ArrayList<String> lista2 = new ArrayList<String>(list);
        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ ubacivanje avatara u rv u onosu na rang
        viewPager = findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(this,slike);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(3);
        //------------------------------------------------------------------------------------------
       //------------------------------------------------------------------------------------------ viewPager

        layout1 = new ConstraintSet();
        layout2 = new ConstraintSet();
        constraintLayout = (ConstraintLayout) findViewById(R.id.constraint_layoutP);
        layout2.clone(this, R.layout.profile_extended);
        layout1.clone(constraintLayout);
        context = this;
        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ animacija layout-a

        ivProfileP = findViewById(R.id.ivProfileP);
        ivBadgeP = findViewById(R.id.ivBadgeP);
        tvFullName = findViewById(R.id.tvFullName);
        tvScore = findViewById(R.id.tvScore);
        tvTrophy = findViewById(R.id.tvTrophy);
        ibScore = findViewById(R.id.ibScore);
        ibTrophy = findViewById(R.id.ibTrophy);
        ibArrow = findViewById(R.id.ibArrow);
        rvAvatar = findViewById(R.id.rvAvatar);
        //layoutManager = new LinearLayoutManager(this);
        layoutManager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        rvAvatar.setLayoutManager(layoutManager);
        rvAvatar.setHasFixedSize(true);
        adapter = new RecyclerAvatar(lista2, this);
        rvAvatar.setAdapter(adapter);
        context = this;
        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ init
        tvFullName.setText(prebacenoIme + " " + prebacenoPrezime);
        tvTrophy.setText(prebacenRang);
        tvScore.setText(prebacenScore);

        String pocetakA = "avatars/";
        String pocetakB = "badges/";
        String slika = pocetakA.concat(prebacenAvatar).concat(".png");
        String slikaBadge = pocetakB.concat(prebacenRang).concat(".png");

        try {
            avatar = Drawable.createFromStream(context.getAssets().open(slika), null);
            badge = Drawable.createFromStream(context.getAssets().open(slikaBadge), null);
        } catch (IOException e) {
            e.printStackTrace();
        }


        Glide
                .with(context)
                .load(avatar)
                //.apply( new RequestOptions().placeholder(R.drawable.placeholder))
                .into(ivProfileP);

        Glide
                .with(context)
                .load(badge)
                //.apply( new RequestOptions().placeholder(R.drawable.placeholder))
                .into(ivBadgeP);

        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ fontovi
        quick_bold = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Bold.ttf");
        quick_light = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Light.ttf");
        quick_medium = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Medium.ttf");
        quick_regular = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Regular.ttf");

        tvFullName.setTypeface(quick_medium);
        tvScore.setTypeface(quick_regular);
        tvTrophy.setTypeface(quick_regular);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_proile);
        getSupportActionBar().hide();
        init();



        ibArrow.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                if (!isOpen) {
                    TransitionManager.beginDelayedTransition(constraintLayout);
                    layout2.applyTo(constraintLayout);
                    isOpen = !isOpen;
                    ibArrow.animate().rotation(180f).setDuration(500);
                    //ibArrow.setImageDrawable(getResources().getDrawable(R.drawable.arrow_down));

                } else {
                    TransitionManager.beginDelayedTransition(constraintLayout);
                    layout1.applyTo(constraintLayout);
                    isOpen = !isOpen;
                    //ibArrow.animate().rotation(-180f).setDuration(500);
                    //ibArrow.setImageDrawable(getResources().getDrawable(R.drawable.arrow_up));
                }
            }
        });


    }

    public void updateAvatar(final String user_id, final String avatar_id) {

         String url_update_avatar = "http://10.0.2.2/wordinary/updateAvatar.php";

        StringRequest stringRequest2 = new StringRequest(Request.Method.POST, url_update_avatar, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Нешто није у реду", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("english",avatar_id);
                params.put("id", user_id);
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest2);

    }

}

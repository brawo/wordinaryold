package com.word.pc.word.Aktivnosti;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tooltip.Tooltip;
import com.word.pc.word.Helperi.MySingleton;
import com.word.pc.word.Misc.RangCalculator;
import com.word.pc.word.R;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.word.pc.word.Main.avatar;

public class Qiuz2 extends AppCompatActivity {

    ProgressBar progressBar;
    TextView tvRangQ1, tvTrenutanScore, tvSledeciNivo, tvImeQ1,
            tvOpcija1, tvOpcija3, tvOpcija2, tvPitanje, tvVerzijaMesanja,tvResenje;
    EditText tvTrazenaRec;
    Button btRefresh, btSubmit;
    LottieAnimationView Level5, Level4, Level3, Level2, Level1, trophy;
    ConstraintLayout quiz2_layout;
    int prebacenRangInt, prebacenScoreInt, granica_za_sledeci_nivo, prebacenHintInt;
    String url_easy = "http://lazyiguanastudios.com/webservices/Wordinary/randomWordQ2_easy.php";
    String url_med = "http://lazyiguanastudios.com/webservices/Wordinary/randomWordQ2_med.php";
    String url_hard = "http://lazyiguanastudios.com/webservices/Wordinary/randomWordQ2_hard.php";
    Animation animShake, animHorizontal;
    int brojacZaHintove = 0;
    Context context;
    ImageView imageView2, imageView3, ivFullScreen;
    int hintIskoriscen = 0;
    private ConstraintSet layout1, layout2;
    private boolean isOpen = false;
    Random random;
    private String prebacenRang;
    private String prebacenScore;
    private String prebacenHint;
    private String randomWords;
    String[] opcije;
    String trazena;
    String prevod;
    int dificulty = 1;
    Typeface quick_bold,quick_light,quick_medium,quick_regular;
    String prebacenID;

    public void init() {
        prebacenID = getIntent().getStringExtra("id");
        Toast.makeText(getApplicationContext(),prebacenID,Toast.LENGTH_SHORT).show();
        progressBar = findViewById(R.id.progressBarQ2);
        tvRangQ1 = findViewById(R.id.tvRangQ2);
        tvTrenutanScore = findViewById(R.id.tvTrenutanScoreQ2);
        tvSledeciNivo = findViewById(R.id.tvSledeciNivoQ2);
        tvImeQ1 = findViewById(R.id.tvImeQ2);
        tvTrazenaRec = findViewById(R.id.tvTrazenaRecQ2);
        tvResenje = findViewById(R.id.tvResenje);
        tvOpcija1 = findViewById(R.id.tvOpcija1Q2);
        tvOpcija3 = findViewById(R.id.tvOpcija3Q2);
        tvOpcija2 = findViewById(R.id.tvOpcija2Q2);
        tvPitanje = findViewById(R.id.tvPitanjeQ2);
        btRefresh = findViewById(R.id.btRefreshQ2);
        btSubmit = findViewById(R.id.btSubmitQ2);
        Level1 = findViewById(R.id.Level1Q2);
        Level2 = findViewById(R.id.Level2Q2);
        Level3 = findViewById(R.id.Level3Q2);
        Level4 = findViewById(R.id.Level4Q2);
        Level5 = findViewById(R.id.Level5Q2);
        trophy = findViewById(R.id.trophyQ2);
        imageView2 = findViewById(R.id.imageView2Q2);
        imageView3 = findViewById(R.id.imageView3Q2);
        ivFullScreen = findViewById(R.id.ivFullScreenQ2);
        quiz2_layout = findViewById(R.id.quiz2_layout);
        tvVerzijaMesanja = findViewById(R.id.tvVerzijaMesanjaQ2);
        animShake = AnimationUtils.loadAnimation(this, R.anim.shake);
        animHorizontal = AnimationUtils.loadAnimation(this, R.anim.shake_horizontal);
        context = this;
        //------------------------------------------------------------------------------------------ progres za progresBar
        prebacenRang = getIntent().getStringExtra("rang");
        prebacenScore = getIntent().getStringExtra("score");
        prebacenHint = getIntent().getStringExtra("hint");
        prebacenHintInt = Integer.parseInt(prebacenHint);
        prebacenRangInt = Integer.parseInt(prebacenRang);
        prebacenScoreInt = Integer.parseInt(prebacenScore);
        granica_za_sledeci_nivo = RangCalculator.setMaxScore(prebacenScoreInt);
        progressBar.setMax(granica_za_sledeci_nivo);
        progressBar.setProgress(prebacenScoreInt);
        tvSledeciNivo.setText("/" + String.valueOf(RangCalculator.setMaxScore(prebacenScoreInt)));
        tvTrenutanScore.setText(prebacenScore);
        tvRangQ1.setText(String.valueOf(prebacenRangInt));
        tvImeQ1.setText(prebacenHint.toString());
        btRefresh.setEnabled(false);
        btRefresh.setAlpha(.5f);
        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ layout animacija

        layout1 = new ConstraintSet();
        layout2 = new ConstraintSet();
        //constraintLayout = (ConstraintLayout) findViewById(R.id.quiz1_layout);
        layout2.clone(this, R.layout.quiz2_extended);
        layout1.clone(quiz2_layout);

        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ fontovi
        quick_bold = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Bold.ttf");
        quick_light = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Light.ttf");
        quick_medium = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Medium.ttf");
        quick_regular = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Regular.ttf");

        tvPitanje.setTypeface(quick_medium);
        tvImeQ1.setTypeface(quick_medium);
        tvOpcija3.setTypeface(quick_medium);
        tvOpcija2.setTypeface(quick_medium);
        tvOpcija1.setTypeface(quick_medium);
        tvTrazenaRec.setTypeface(quick_medium);
        tvRangQ1.setTypeface(quick_medium);
        tvSledeciNivo.setTypeface(quick_medium);
        tvTrenutanScore.setTypeface(quick_medium);
        btRefresh.setTypeface(quick_medium);
        btSubmit.setTypeface(quick_medium);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qiuz2);
        getSupportActionBar().hide();
        init();
        initDificulty(dificulty);
        randomWord(dificulty);

        Glide
                .with(context)
                .load(avatar)
                .apply(new RequestOptions().centerInside())
                .into(imageView2);

        ivFullScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isOpen) {
                    TransitionManager.beginDelayedTransition(quiz2_layout);
                    layout2.applyTo(quiz2_layout);
                    isOpen = !isOpen;
                }else {
                    TransitionManager.beginDelayedTransition(quiz2_layout);
                    layout1.applyTo(quiz2_layout);
                    isOpen = !isOpen;
                }
            }
        });

        btRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                randomWord(dificulty);
                if (brojacZaHintove >9) {
                    brojacZaHintove = 0;
                    Level1.setProgress(0f);
                    Level2.setProgress(0f);
                    Level3.setProgress(0f);
                    Level4.setProgress(0f);
                    Level5.setProgress(0f);
                }
                animShake.cancel();
                btSubmit.setEnabled(true);
                btSubmit.setAlpha(1f);
                btRefresh.setEnabled(false);
                btRefresh.setAlpha(.5f);
                tvTrazenaRec.setEnabled(true);
                tvTrazenaRec.setAlpha(1f);
                tvTrazenaRec.setText("");
                tvTrazenaRec.setHint("");
                imageView3.setEnabled(true);
                imageView3.setAlpha(1f);
                tvResenje.setVisibility(View.INVISIBLE);

            }
        });

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitAnswer(tvTrazenaRec.getText().toString());
            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLetters();
            }
        });
    }



    public void changeDificuly(View view) {
        int element_id = view.getId();
        if(btRefresh.getAlpha()<1f){
            btSubmit.startAnimation(animHorizontal);
        }else {
            switch (element_id) {
                case R.id.tvOpcija1Q2:
                    tvOpcija1.setBackground(getResources().getDrawable(R.drawable.rv_avatar_background));
                    tvOpcija2.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                    tvOpcija3.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                    dificulty = 1;
                    break;
                case R.id.tvOpcija2Q2:
                    tvOpcija1.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                    tvOpcija2.setBackground(getResources().getDrawable(R.drawable.rv_avatar_background));
                    tvOpcija3.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                    dificulty = 2;
                    break;
                case R.id.tvOpcija3Q2:
                    tvOpcija1.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                    tvOpcija2.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                    tvOpcija3.setBackground(getResources().getDrawable(R.drawable.rv_avatar_background));
                    dificulty = 3;
                    break;

            }
        }
    }

    String changeString(String s) {
        char[] characters = s.toCharArray();
        switch (dificulty) {
            case 1:
                int rand = (int) (Math.random() * s.length());
                characters[rand] = '_';
                break;
            case 2:
                int rand2 = (int) (Math.random() * s.length());
                int rand3 = (int) (Math.random() * s.length());
                int rand4 = (int) (Math.random() * s.length());
                characters[rand2] = '_';
                characters[rand3] = '_';
                characters[rand4] = '_';
                break;
            case 3:
                int rand5 = (int) (Math.random() * s.length());
                int rand6 = (int) (Math.random() * s.length());
                int rand7 = (int) (Math.random() * s.length());
                int rand8 = (int) (Math.random() * s.length());
                int rand9 = (int) (Math.random() * s.length());
                characters[rand5] = '_';
                characters[rand6] = '_';
                characters[rand7] = '_';
                characters[rand8] = '_';
                characters[rand9] = '_';
                break;

        }

        return new String(characters);
    }

    private void randomWord(final int dificulty) {
        this.dificulty=dificulty;

        switch (dificulty){
            case 1:
                StringRequest stringRequest1 = new StringRequest(Request.Method.POST, url_easy, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        randomWords = response;
                        opcije = randomWords.split("--");
                        trazena = opcije[0];
                        prevod = opcije[1];
                        tvPitanje.setText("Prevod reci " + trazena.toUpperCase() + " glasi:");
                        tvVerzijaMesanja.setText(changeString(prevod));
                        tvResenje.setText(prevod);


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Нешто није у реду", Toast.LENGTH_SHORT).show();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("user_id", prebacenID);
                        return params;
                    }
                };
                MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest1);
                break;
            case 2:
                StringRequest stringRequest2 = new StringRequest(Request.Method.POST, url_med, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        randomWords = response;
                        randomWords = response;
                        opcije = randomWords.split("--");
                        trazena = opcije[0];
                        prevod = opcije[1];
                        tvPitanje.setText("Prevod reci " + trazena.toUpperCase() + " glasi:");
                        tvVerzijaMesanja.setText(changeString(prevod));
                        tvResenje.setText(prevod);

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Нешто није у реду", Toast.LENGTH_SHORT).show();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("user_id", prebacenID);
                        return params;
                    }
                };
                MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest2);
                break;
            case 3:
                StringRequest stringRequest3 = new StringRequest(Request.Method.POST, url_hard, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        randomWords = response;
                        randomWords = response;
                        opcije = randomWords.split("--");
                        trazena = opcije[0];
                        prevod = opcije[1];
                        tvPitanje.setText("Prevod reci " + trazena.toUpperCase() + " glasi:");
                        tvVerzijaMesanja.setText(changeString(prevod));
                        tvResenje.setText(prevod);

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Нешто није у реду", Toast.LENGTH_SHORT).show();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("user_id", prebacenID);
                        return params;
                    }
                };
                MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest3);
                break;
        }

    }

    public void initDificulty(int dificulty){
        switch (dificulty){
            case 1:
                tvOpcija1.setBackground(getResources().getDrawable(R.drawable.rv_avatar_background));
                tvOpcija2.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                tvOpcija3.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                dificulty = 1;
                break;
            case 2:
                tvOpcija1.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                tvOpcija2.setBackground(getResources().getDrawable(R.drawable.rv_avatar_background));
                tvOpcija3.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                dificulty = 2;
                break;
            case 3:
                tvOpcija1.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                tvOpcija2.setBackground(getResources().getDrawable(R.drawable.rv_item_background));
                tvOpcija3.setBackground(getResources().getDrawable(R.drawable.rv_avatar_background));
                dificulty = 3;
                break;

        }
    }

    public void submitAnswer(String answer) {

        if (answer.toString().equals(prevod) && hintIskoriscen == 0) {
            tvTrazenaRec.setEnabled(false);
            /*tvOpcija1.setEnabled(false);
            tvOpcija2.setEnabled(false);
            tvOpcija3.setEnabled(false);*/
            tvTrazenaRec.setAlpha(.5f);
            /*tvOpcija1.setAlpha(.5f);
            tvOpcija2.setAlpha(.5f);
            tvOpcija3.setAlpha(.5f);*/
            btRefresh.startAnimation(animShake);
            btSubmit.setEnabled(false);
            btSubmit.setAlpha(.5f);
            btRefresh.setEnabled(true);
            btRefresh.setAlpha(1f);

            prebacenScoreInt++;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                progressBar.setProgress(prebacenScoreInt, true);
            }
            tvTrenutanScore.setText(String.valueOf(prebacenScoreInt));
            if (prebacenScoreInt > granica_za_sledeci_nivo) {

                //----------------------------------------------------------------------------------

                int cx = trophy.getWidth() / 2;
                int cy = trophy.getHeight() / 2;

                // get the final radius for the clipping circle
                float finalRadius = (float) Math.hypot(cx, cy);

                // create the animator for this view (the start radius is zero)
                Animator anim =
                        ViewAnimationUtils.createCircularReveal(trophy, cx, cy, 0, finalRadius);

                // make the view visible and start the animation
                trophy.setVisibility(View.VISIBLE);

                anim.start();
                trophy.playAnimation();
                trophy.addAnimatorListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        int cx = trophy.getWidth() / 2;
                        int cy = trophy.getHeight() / 2;

                        float initialRadius = (float) Math.hypot(cx, cy);

                        Animator anim =
                                ViewAnimationUtils.createCircularReveal(trophy, cx, cy, initialRadius, 0);

                        anim.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);

                                trophy.setVisibility(View.INVISIBLE);

                            }
                        });

                        anim.start();
                    }
                });


                //---------------------------------------------------------------------------------- todo trofej json animacija
                tvSledeciNivo.setText("/" + String.valueOf(RangCalculator.setMaxScore(prebacenScoreInt)));
                prebacenRangInt++;
                tvRangQ1.setText(String.valueOf(prebacenRangInt));
                granica_za_sledeci_nivo = RangCalculator.setMaxScore(prebacenScoreInt);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    progressBar.setProgress(prebacenScoreInt, true);
                }
                progressBar.setMax(granica_za_sledeci_nivo);
            }

            //---------------------------------------------------------------------------------- hintovi!!
            brojacZaHintove++;
            Toast.makeText(getApplicationContext(),"brojzahintove je: " + brojacZaHintove,Toast.LENGTH_SHORT);
            switch (brojacZaHintove) {
                case 2:
                    Level1.playAnimation();
                    break;
                case 4:
                    Level2.playAnimation();
                    break;
                case 6:
                    Level2.playAnimation();
                    Level3.playAnimation();
                    break;
                case 8:
                    Level3.playAnimation();
                    Level4.playAnimation();
                    break;
                case 9:
                    Level1.playAnimation();
                    Level2.playAnimation();
                    Level3.playAnimation();
                    Level4.playAnimation();
                    Tooltip tooltip = new Tooltip.Builder(Level4)
                            .setText("jos jedan i hint!")
                            .setTextColor(Color.WHITE)
                            .setGravity(Gravity.TOP)
                            .setCornerRadius(8f)
                            .setDismissOnClick(true)
                            .setCancelable(true)
                            .setBackgroundColor(getResources().getColor(R.color.zuta))
                            .show();
                    break;
                case 10:
                    Level1.setRepeatCount(1);
                    Level1.playAnimation();
                    Level2.setRepeatCount(1);
                    Level2.playAnimation();
                    Level3.setRepeatCount(1);
                    Level3.playAnimation();
                    Level4.setRepeatCount(1);
                    Level4.playAnimation();
                    Level5.setRepeatCount(1);
                    Level5.playAnimation();
                    prebacenHintInt++;
                    tvImeQ1.setText(String.valueOf(prebacenHintInt));
                    Level5.addAnimatorListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            Level1.setProgress(0f);
                            Level2.setProgress(0f);
                            Level3.setProgress(0f);
                            Level4.setProgress(0f);
                            Level5.setProgress(0f);
                        }
                    });

                    break;
            }

        } else if (answer.toString().equals(prevod) && hintIskoriscen > 0) {
            tvTrazenaRec.setEnabled(false);
            tvTrazenaRec.setAlpha(.5f);
            btRefresh.startAnimation(animShake);
            brojacZaHintove = 0;
            hintIskoriscen = 0;
            Level1.setProgress(0f);
            Level2.setProgress(0f);
            Level3.setProgress(0f);
            Level4.setProgress(0f);
            Level5.setProgress(0f);
            btSubmit.setEnabled(false);
            btSubmit.setAlpha(.5f);
            btRefresh.setEnabled(true);
            btRefresh.setAlpha(1f);
            Toast.makeText(getApplicationContext(), "tacno,nema poena,brise se level", Toast.LENGTH_SHORT).show();

        } else {
            imageView3.setEnabled(false);
            imageView3.setAlpha(.5f);
            brojacZaHintove = 0;
            tvTrazenaRec.setEnabled(false);
            Level1.setProgress(0f);
            Level2.setProgress(0f);
            Level3.setProgress(0f);
            Level4.setProgress(0f);
            Level5.setProgress(0f);
            Toast.makeText(getApplicationContext(), "netacno", Toast.LENGTH_SHORT).show();
            btRefresh.startAnimation(animShake);
            btSubmit.setEnabled(false);
            btSubmit.setAlpha(.5f);
            btRefresh.setEnabled(true);
            btRefresh.setAlpha(1f);
            tvResenje.setVisibility(View.VISIBLE);
        }
    }

    public void showLetters(){
        if (btSubmit.getAlpha() < 1f) {
            btRefresh.startAnimation(animShake);
        } else if (hintIskoriscen == 0) {
            if (prebacenHintInt > 0) {

                tvTrazenaRec.setText("");
                tvTrazenaRec.setHint(tvVerzijaMesanja.getText().toString());
                hintIskoriscen++;
                prebacenHintInt--;

                imageView3.setAlpha(.5f);
                imageView3.setEnabled(false);
            } else {
                Toast.makeText(getApplicationContext(), "nema vise hintova", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

package com.word.pc.word.Adapteri;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.word.pc.word.R;

public class ViewPagerAdapter extends PagerAdapter {
    Activity activity;
    int[] images;
    LayoutInflater layoutInflater;

    public ViewPagerAdapter(Activity activity, int[] images) {
        this.activity = activity;
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject( View view,  Object o) {
        return view==o;
    }


    @Override
    public Object instantiateItem( ViewGroup container, int position) {
        //layoutInflater = (LayoutInflater) activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = LayoutInflater.from(container.getContext()).inflate(R.layout.view_pager_item,container,false);
        ImageView imageView = itemView.findViewById(R.id.imageView);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int with = displayMetrics.widthPixels;
        imageView.setMaxHeight(height);
        imageView.setMaxWidth(with);

        Glide
                .with(activity.getApplicationContext())
                .load(images[position])
                //.apply(RequestOptions.placeholderOf(R.drawable.placeholder))
                .into(imageView);


        container.addView(imageView);

        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);

    }

}

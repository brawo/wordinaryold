package com.word.pc.word.Adapteri;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.word.pc.word.Aktivnosti.LeaderBoard;
import com.word.pc.word.Misc.DbContract;
import com.word.pc.word.Modeli.User;
import com.word.pc.word.R;

import java.io.IOException;
import java.util.ArrayList;

public class RecyclerLeader extends RecyclerView.Adapter<RecyclerLeader.MyViewHolder> {

    private ArrayList<User> arrayList;
    private Context context;
    private int lastPosition = -1;

    public RecyclerLeader(ArrayList<User> arrayList,Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerLeader.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_leaderboard,parent,false);
        return new RecyclerLeader.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerLeader.MyViewHolder holder, final int position) {

        holder.tvImeRv.setText(arrayList.get(position).getName());
        holder.tvMestoRv.setText(String.valueOf(position+1));
        holder.tvScoreRv.setText(String.valueOf(arrayList.get(position).getScore()));
        holder.tvIdLeader.setText(String.valueOf(arrayList.get(position).getId()));
        String id = String.valueOf(arrayList.get(position).getId());

        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ fontovi

        holder.tvImeRv.setTypeface(holder.quick_regular);
        holder.tvMestoRv.setTypeface(holder.quick_medium);
        holder.tvScoreRv.setTypeface(holder.quick_medium);

        if(id.equals(LeaderBoard.prebacenID)){
            holder.tvImeRv.setTextColor(holder.context.getResources().getColor(R.color.zuta));
            holder.tvImeRv.setTypeface(holder.quick_bold);
        }

        holder.j = position+1;
        String pocetak = "avatars/";
        String kraj = String.valueOf(arrayList.get(position).getAvatar());
        String ext = ".png";
        String slika = pocetak.concat(kraj).concat(ext);


        try {
            holder.avatar = Drawable.createFromStream(holder.context.getAssets().open(slika), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Glide
                .with(context)
                .load(holder.avatar)
                .apply( new RequestOptions().optionalCircleCrop())
                .into(holder.ivAvatarRv);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {

        ImageView ivAvatarRv;
        TextView tvMestoRv,tvImeRv,tvScoreRv,tvIdLeader;
        Context context;
        Drawable avatar;
        ConstraintLayout rvItem_leader;
        int j;
        Typeface quick_bold,quick_light,quick_medium,quick_regular;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();
            rvItem_leader = itemView.findViewById(R.id.rvItem_leader);
            tvIdLeader = itemView.findViewById(R.id.tvIdLeader);
            ivAvatarRv = itemView.findViewById(R.id.ivAvatarRv);
            tvMestoRv = itemView.findViewById(R.id.tvMestoRv);
            tvImeRv = itemView.findViewById(R.id.tvImeRv);
            tvScoreRv = itemView.findViewById(R.id.tvScoreRv);
            avatar = null;
            j = 0;

            //-----------------------------------------------
            quick_bold = Typeface.createFromAsset(context.getAssets(),"fonts/Quicksand-Bold.ttf");
            quick_light = Typeface.createFromAsset(context.getAssets(),"fonts/Quicksand-Light.ttf");
            quick_medium = Typeface.createFromAsset(context.getAssets(),"fonts/Quicksand-Medium.ttf");
            quick_regular = Typeface.createFromAsset(context.getAssets(),"fonts/Quicksand-Regular.ttf");
        }
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
package com.word.pc.word.Aktivnosti;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.schibsted.spain.parallaxlayerlayout.ParallaxLayerLayout;
import com.schibsted.spain.parallaxlayerlayout.SensorTranslationUpdater;
import com.word.pc.word.Helperi.MySingleton;
import com.word.pc.word.Main;
import com.word.pc.word.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {
    SensorTranslationUpdater sensorTranslationUpdater;
    ParallaxLayerLayout parallaxLayout;
    ConstraintLayout login_layout;
    TextView tvNaslovL,tvObavestenje1,tvObavestenje2,rezultat;
    EditText etUsernameL,etPasswordL;
    CheckBox checkBox;
    Button btLogin;
    String url_login = "http://lazyiguanastudios.com/webservices/Wordinary/login.php";
    Typeface quick_bold,quick_light,quick_medium,quick_regular;

    //---------------------------------------------------------------------------------------------- cuvanje cred
    String username, password, id;
    SharedPreferences loginPreferences;
    SharedPreferences.Editor loginPrefsEditor;
    Boolean saveLogin;
    //----------------------------------------------------------------------------------------------

    public void init(){
        parallaxLayout = (ParallaxLayerLayout) findViewById(R.id.parallax);
        sensorTranslationUpdater = new SensorTranslationUpdater(this);
        parallaxLayout.setTranslationUpdater(sensorTranslationUpdater);
        tvNaslovL = findViewById(R.id.tvNaslovL);
        tvObavestenje1 = findViewById(R.id.tvObavestenje1);
        tvObavestenje2 = findViewById(R.id.tvObavestenje2);
        etUsernameL = findViewById(R.id.etUsernameL);
        etPasswordL = findViewById(R.id.etPasswordL);
        btLogin = findViewById(R.id.btLogin);
        rezultat = findViewById(R.id.rezultat);
        login_layout = findViewById(R.id.login_layout);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ fontovi
        quick_bold = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Bold.ttf");
        quick_light = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Light.ttf");
        quick_medium = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Medium.ttf");
        quick_regular = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Regular.ttf");

        tvNaslovL.setTypeface(quick_light);
        tvObavestenje1.setTypeface(quick_regular);
        tvObavestenje2.setTypeface(quick_bold);
        etUsernameL.setTypeface(quick_regular);
        etPasswordL.setTypeface(quick_regular);
        btLogin.setTypeface(quick_bold);
        checkBox.setTypeface(quick_regular);

        //------------------------------------------------------------------------------------------ cuvanje user/pass

        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();

        saveLogin = loginPreferences.getBoolean("saveLogin", false);
        if (saveLogin == true) {
            etUsernameL.setText(loginPreferences.getString("username", ""));
            //etUsername.setText(loginPreferences.getString(getString(R.string.username), ""));
            etPasswordL.setText(loginPreferences.getString("password", ""));
            //etPassword.setText(loginPreferences.getString(getString(R.string.password), ""));
            rezultat.setText(loginPreferences.getString("id", ""));
            //rezultat.setText(loginPreferences.getString(getString(R.string.id), ""));
            checkBox.setChecked(true);
        }

        parallaxLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sensorTranslationUpdater.reset();
            }
        });


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        init();

        tvObavestenje2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),Register.class);
                startActivity(intent);
            }
        });

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = etUsernameL.getText().toString();
                password = etPasswordL.getText().toString();
                id = rezultat.getText().toString();

                if (checkBox.isChecked()) {
                    loginPrefsEditor.putBoolean("saveLogin", true);
                    loginPrefsEditor.putString("username", username);
                    //loginPrefsEditor.putString(getString(R.string.username), username);
                    loginPrefsEditor.putString("password", password);
                    //loginPrefsEditor.putString(getString(R.string.password), password);
                    loginPrefsEditor.putString("id", id);
                    //loginPrefsEditor.putString(getString(R.string.id), id);
                    loginPrefsEditor.commit();

                    LoginUser();

                } else {
                    loginPrefsEditor.clear();
                    loginPrefsEditor.commit();

                    LoginUser();

                }
            }
        });
    }

    public void LoginUser() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_login, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("netacn")) {
                    Snackbar snackbar = Snackbar.make(login_layout, "Wrong username or password!", Snackbar.LENGTH_SHORT);
                    View snkview = snackbar.getView();
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) snkview.getLayoutParams();
                    params.gravity = Gravity.TOP;
                    snkview.setLayoutParams(params);
                    snkview.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.prljava));
                    snackbar.show();

                } else {

                        try {

                            JSONObject jsonObject = new JSONObject(response.toString());

                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            JSONObject jsonRow = jsonArray.getJSONObject(0);

                            String resultStr = jsonRow.getString("id");
                            rezultat.setText(resultStr);
                            Intent intent = new Intent(Login.this,Main.class);
                            intent.putExtra("id",resultStr);
                            startActivity(intent);
                            finish();
                        } catch (JSONException e) {

                        }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar snackbar = Snackbar.make(login_layout, "Something went wrong!", Snackbar.LENGTH_SHORT);
                View snkview = snackbar.getView();
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) snkview.getLayoutParams();
                params.gravity = Gravity.TOP;
                snkview.setLayoutParams(params);
                snkview.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.prljava));
                snackbar.show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", etUsernameL.getText().toString());
                params.put("password", etPasswordL.getText().toString());
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorTranslationUpdater.registerSensorManager();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorTranslationUpdater.unregisterSensorManager();
    }


}

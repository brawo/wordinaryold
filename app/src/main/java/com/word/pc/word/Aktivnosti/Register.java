package com.word.pc.word.Aktivnosti;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.schibsted.spain.parallaxlayerlayout.ParallaxLayerLayout;
import com.schibsted.spain.parallaxlayerlayout.SensorTranslationUpdater;
import com.word.pc.word.Helperi.MySingleton;
import com.word.pc.word.R;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.word.pc.word.Misc.RangCalculator.proper_noun;
import static com.word.pc.word.Misc.RangCalculator.proper_noun2;

public class Register extends AppCompatActivity {

    SensorTranslationUpdater sensorTranslationUpdater;
    ParallaxLayerLayout parallaxLayoutR;
    String url_register = "http://lazyiguanastudios.com/webservices/Wordinary/register.php";
    TextView tvNaslovR,tvGenerate;
    EditText etUsernameR,etPasswordR,etNameR,etLastnameR;
    Button btSignUp;
    Typeface quick_bold,quick_light,quick_medium,quick_regular;

    public void init(){
        parallaxLayoutR = (ParallaxLayerLayout) findViewById(R.id.parallaxR);
        sensorTranslationUpdater = new SensorTranslationUpdater(this);
        parallaxLayoutR.setTranslationUpdater(sensorTranslationUpdater);
        tvNaslovR = findViewById(R.id.tvNaslovR);
        tvGenerate = findViewById(R.id.tvGenerate);
        etNameR = findViewById(R.id.etNameR);
        etLastnameR = findViewById(R.id.etLastnameR);
        etUsernameR = findViewById(R.id.etUsernameR);
        etPasswordR = findViewById(R.id.etPasswordR);
        btSignUp = findViewById(R.id.btSignUp);
        //------------------------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------ fontovi
        quick_bold = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Bold.ttf");
        quick_light = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Light.ttf");
        quick_medium = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Medium.ttf");
        quick_regular = Typeface.createFromAsset(getAssets(),"fonts/Quicksand-Regular.ttf");

        tvNaslovR.setTypeface(quick_light);
        tvGenerate.setTypeface(quick_regular);
        etLastnameR.setTypeface(quick_regular);
        etNameR.setTypeface(quick_regular);
        etPasswordR.setTypeface(quick_regular);
        etUsernameR.setTypeface(quick_regular);
        btSignUp.setTypeface(quick_bold);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();
        init();
        btSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });

        tvGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etUsernameR.setText(usernameGenerator());
            }
        });
    }

    private void registerUser() {

        StringRequest stringRequest3 = new StringRequest(Request.Method.POST, url_register, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("zauzeto"))
                {
                    Toast.makeText(getApplicationContext(),"username taken",Toast.LENGTH_SHORT).show();
                    tvGenerate.setVisibility(View.VISIBLE);

                }
                else if (response.contains("tacno"))
                {
                    Toast.makeText(getApplicationContext(),"Registration succesfull!",Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Register.this,Login.class);
                    startActivity(intent);
                    finish();
                }else {
                    Toast.makeText(Register.this,response,Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Нешто није у реду",Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("name",etNameR.getText().toString());
                params.put("lastname",etLastnameR.getText().toString());
                params.put("username",etUsernameR.getText().toString());
                params.put("password",etPasswordR.getText().toString());
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest3);
    }

    public String usernameGenerator() {
        Random random = new Random();
        int index = random.nextInt(proper_noun.length);
        int index2 = random.nextInt(proper_noun2.length);
        return (proper_noun[index]+proper_noun2[index2]);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorTranslationUpdater.registerSensorManager();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorTranslationUpdater.unregisterSensorManager();
    }
}
